const path = require("path");
const {
  matchesPerYear,
  winnerTeamsPerYear,
  extraRunConcededPerTeam2016,
  economicalBowlers,
} = require(path.join(__dirname, "../server/ipl"));

/* Test cases for first question */
{
  let dummy_data = [
    {
      season: "2016",
    },
    {
      season: "2017",
    },
    {
      season: "2016",
    },
  ];
  let result = { 2016: 2, 2017: 1 };

  test("should be a function", () => {
    expect(typeof matchesPerYear).toBe("function");
  });

  test("should return object", () => {
    expect(typeof matchesPerYear(dummy_data)).toBe("object");
  });

  test("should return the season count", () => {
    expect(matchesPerYear(dummy_data)).toEqual(result);
  });
}

/* --------------------------------------------------------------------------------------- */

/* Test cases for second question */
{
  let dummy_data = [
    {
      winner: "a",
      season: "2015",
    },
    {
      winner: "a",
      season: "2016",
    },
    {
      winner: "a",
      season: "2016",
    },
    {
      winner: "c",
      season: "2016",
    },
    {
      winner: "b",
      season: "2016",
    },
    {
      winner: "a",
      season: "2017",
    },
    {
      winner: "b",
      season: "2017",
    },
  ];

  let result = {
    a: { 2015: 1, 2016: 2, 2017: 1 },
    b: {
      2016: 1,
      2017: 1,
    },
    c: {
      2016: 1,
    },
  };

  test("should be a function", () => {
    expect(typeof winnerTeamsPerYear).toBe("function");
  });

  test("should return object", () => {
    expect(typeof winnerTeamsPerYear(dummy_data)).toBe("object");
  });

  test("should return the winner count per season", () => {
    expect(winnerTeamsPerYear(dummy_data)).toEqual(result);
  });
}
/* -------------------------------------------------------------------------------------- */
/* Test cases for third question */

{
  let dummy_data = [
    {
      match_id: 577,
      bowling_team: "team1",
      extra_runs: 5,
    },
    { match_id: 578, bowling_team: "team2", extra_runs: 4 },
    { match_id: 579, bowling_team: "team1", extra_runs: 3 },
  ];
  let matchDummyData = [
    {
      id: "577",
      season: "2016",
    },
    {
      id: "578",
      season: "2016",
    },
    {
      id: "579",
      season: "2016",
    },
  ];
  let result = { team1: 8, team2: 4 };

  test("should be a function", () => {
    expect(typeof extraRunConcededPerTeam2016).toBe("function");
  });

  test("should return object", () => {
    expect(
      typeof extraRunConcededPerTeam2016(matchDummyData, dummy_data, "2016")
    ).toBe("object");
  });

  test("should return the extra run conceded per team in 2016", () => {
    expect(
      extraRunConcededPerTeam2016(matchDummyData, dummy_data, "2016")
    ).toEqual(result);
  });
}

/* ---------------------------------------------------------------- */
/* Test cases for fourth question */
{
  let dummy_data = [
    {
      match_id: 577,
      bowler: "raj",
      total_runs: 5,
      legbye_runs: 0,
      bye_runs: 0,
    },
    {
      match_id: 582,
      bowler: "alok",
      total_runs: 1,
      legbye_runs: 0,
      bye_runs: 0,
    },
    {
      match_id: 577,
      bowler: "raj",
      total_runs: 1,
      legbye_runs: 0,
      bye_runs: 0,
    },
    {
      match_id: 577,
      bowler: "raj",
      total_runs: 1,
      legbye_runs: 0,
      bye_runs: 0,
    },
    {
      match_id: 577,
      bowler: "raj",
      total_runs: 2,
      legbye_runs: 0,
      bye_runs: 0,
    },
    {
      match_id: 582,
      bowler: "alok",
      total_runs: 2,
      legbye_runs: 0,
      bye_runs: 0,
    },
    {
      match_id: 583,
      bowler: "aman",
      total_runs: 1,
      legbye_runs: 0,
      bye_runs: 0,
    },
  ];

  let matchDummyData = [
    {
      id: "577",
      season: "2016",
    },
    {
      id: "578",
      season: "2016",
    },
    {
      id: "579",
      season: "2016",
    },
    {
      id: "580",
      season: "2016",
    },
    {
      id: "581",
      season: "2016",
    },
    {
      id: "582",
      season: "2016",
    },
    {
      id: "583",
      season: "2016",
    },
  ];

  let result = [
    ["aman", "6.00"],
    ["alok", "18.00"],
    ["raj", "54.00"],
  ];

  test("should be a function", () => {
    expect(typeof economicalBowlers).toBe("function");
  });

  test("should return array", () => {
    expect(typeof economicalBowlers(matchDummyData, dummy_data, "2016")).toBe(
      "object"
    );
  });

  test("should return the top ten economical bowlers in 2016", () => {
    expect(economicalBowlers(matchDummyData, dummy_data, "2016")).toEqual(
      result
    );
  });
}
