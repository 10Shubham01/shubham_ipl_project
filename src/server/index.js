const fs = require("fs");
const path = require("path");
/*---------------------------Importing Data-------------------- */
try {
  const matchesData = fs.readFileSync(
    path.join(__dirname, "../data/matches.json")
  );
  const deliveriesData = fs.readFileSync(
    path.join(__dirname, "../data/deliveries.json")
  );

  /* ----------Converting text Into JavaScript Objects---------- */

  const dataFromMatches = JSON.parse(matchesData);
  const dataFromDeliveries = JSON.parse(deliveriesData);

  /* Importing modules from ipl.js */

  const {
    matchesPerYear,
    winnerTeamsPerYear,
    extraRunConcededPerTeam2016,
    economicalBowlers,
  } = require("./ipl");

  /* Invokeing modules, converting JavaScript Object into text and dumping into Output folder */

  /* Number of matches played per year for all the years in IPL. */
  fs.writeFileSync(
    "../public/output/matchesPerYear.json",
    JSON.stringify(matchesPerYear(dataFromMatches), null, 2)
  );

  /* Number of matches won per team per year in IPL. */
  fs.writeFileSync(
    "../public/output/winnerTeamsPerYear.json",
    JSON.stringify(winnerTeamsPerYear(dataFromMatches), null, 2)
  );

  /* Extra runs conceded per team in the year 2016 */
  fs.writeFileSync(
    "../public/output/extraRunConcededPerTeam2016.json",
    JSON.stringify(
      extraRunConcededPerTeam2016(dataFromMatches, dataFromDeliveries, "2016"),
      null,
      2
    )
  );

  /* Top 10 economical bowlers in the year 2015 */
  fs.writeFileSync(
    "../public/output/economicalBowlers.json",
    JSON.stringify(
      economicalBowlers(dataFromMatches, dataFromDeliveries, "2015"),
      null,
      2
    )
  );

  console.log("All Data saved into ../public/output");
} catch (e) {
  console.log(e.message);
}
