/* -----------Function Definations-------------- */
/* Number of matches played per year for all the years in IPL. */

const matchesPerYear = (matchData) => {
  if (Array.isArray(matchData) == false) {
    console.log("Inappropriate Data has been passed");
  } else {
    try {
      let dataMatchesPerYear = matchData.reduce((acc, cur) => {
        if (acc.hasOwnProperty(cur.season)) acc[cur.season] += 1;
        else acc[cur.season] = 1;
        return acc;
      }, {});
      return dataMatchesPerYear;
    } catch (e) {
      console.log(e.message);
    }
  }
};

/* -------------------------------------------------- */
/* Number of matches won per team per year in IPL. */

const winnerTeamsPerYear = (matchData) => {
  if (Array.isArray(matchData) == false) {
    console.log("Inappropriate Data has been passed");
  } else {
    try {
      let dataWinnerTeamsPerYear = matchData.reduce((acc, cur) => {
        if (acc.hasOwnProperty(cur.winner)) {
          if (acc[cur.winner].hasOwnProperty(cur.season))
            acc[cur.winner][cur.season] += 1;
          else acc[cur.winner][cur.season] = 1;
        } else {
          acc[cur.winner] = {};
          acc[cur.winner][cur.season] = 1;
        }
        return acc;
      }, {});
      return dataWinnerTeamsPerYear;
    } catch (e) {
      console.log(e.message);
    }
  }
};

/* ------------------------------------------------------------------------------------ */
/* Extra runs conceded per team in the year 2016 */

const extraRunConcededPerTeam2016 = (matchData, deliveriesData, whichYear) => {
  if (
    Array.isArray(matchData) == false ||
    Array.isArray(deliveriesData) == false ||
    whichYear == null
  ) {
    console.log("Inappropriate Data has been passed");
  } else {
    try {
      [f, ...l] = matchData.filter((e) => e.season === whichYear);
      let x = l.pop().id;
      let extraRunConceded = deliveriesData
        .filter((data) => {
          return (
            parseInt(data.match_id) >= parseInt(f.id) &&
            parseInt(data.match_id) <= parseInt(x)
          );
        })
        .reduce((acc, cur) => {
          let extra = parseInt(cur.extra_runs);
          if (acc.hasOwnProperty(cur.bowling_team))
            acc[cur.bowling_team] += extra;
          else acc[cur.bowling_team] = extra;
          return acc;
        }, {});

      return extraRunConceded;
    } catch (e) {
      console.log(e.message);
    }
  }
};

/* ---------------------------------------------------------------------------------- */
/* Top 10 economical bowlers in the year 2015 */

const economicalBowlers = (matchesData, deliveriesData, whichYear) => {
  if (
    Array.isArray(matchesData) == false ||
    Array.isArray(deliveriesData) == false ||
    whichYear == null
  ) {
    console.log("Inappropriate Data has been passed");
  } else {
    try {
      [f, ...l] = matchesData.filter((e) => e.season === whichYear);
      let x = l.pop().id;
      let bowlers = deliveriesData
        .filter((data) => {
          return (
            parseInt(data.match_id) >= parseInt(f.id) &&
            parseInt(data.match_id) <= parseInt(x)
          );
        })
        .reduce((acc, cur) => {
          let tRun = parseInt(
            cur.total_runs - (cur.bye_runs + cur.legbye_runs)
          );
          if (acc.hasOwnProperty(cur.bowler)) {
            acc[cur.bowler]["runs"] += tRun;
            if (parseInt(cur.ball) <= 6) {
              acc[cur.bowler]["balls"] += 1;
            }
          } else {
            acc[cur.bowler] = {
              runs: tRun,
              balls: 1,
            };
          }
          return acc;
        }, {});
      let keys = Object.keys(bowlers).reduce((acc, cur) => {
        acc[cur] = ((bowlers[cur].runs * 6) / bowlers[cur].balls).toFixed(2);
        return acc;
      }, {});
      let result = Object.entries(keys)
        .sort((aa, bb) => {
          return aa[1] - bb[1];
        })
        .slice(0, 10);

      return result;
    } catch (e) {
      console.log(e.message);
    }
  }
};

/* -------------------Exporting mmodules------------- */

module.exports = {
  matchesPerYear,
  winnerTeamsPerYear,
  extraRunConcededPerTeam2016,
  economicalBowlers,
};
